//Inicio

namespace exercicio_2
{
    let nota1, nota2, nota3, peso1, peso2, peso3: number;
    let media: number;
    let pesos: number;

    nota1 = 5;
    nota2 = 8;
    nota3 = 7;
    peso1 = 2;
    peso2 = 3;
    peso3 = 5;
    pesos = peso1 + peso2 + peso3;

    media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / pesos;

    console.log("A media ponderada das notas é: " + media);

    if(media >=8 && media <=10){console.log("A media do aluno terá o Conceito A " + media)}
    else if(media >=7 && media <8){console.log("A media do aluno terá o Conceito B " + media)}
    else if(media >=6 && media <7){console.log("A media do aluno terá o Conceito C " + media)}
    else if(media >=5 && media <6){console.log("A media do aluno terá o Conceito D " + media)}
    else if(media >=0 && media <5){console.log("A media do aluno terá o Conceito E " + media)}

}