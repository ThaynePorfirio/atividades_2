namespace exemplos_condicoes {
    let idade: number = 10;
    
    if(idade >= 18)
    {//inicio bloco
        console.log("Pode dirigir");
    }//fim bloco
    else
    {
        console.log("Não pode dirigir");
    }

    //Ternario
    idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode dirigir");

}