//Testando o operador OU - ||
namespace operador_Ou {

    let idade = 16;
    let maiorIdade = idade > 18;
    let possuiAutorizacaoDosPais = false;

    let podeBeber = maiorIdade || possuiAutorizacaoDosPais;
    console.log(podeBeber); // true
}